const up = "up";
const down = "down";
const stopped = "stopped";

myElevators = [];
floorsWaiting = [];

({
    init: function (elevators, floors) {
        elevators.forEach((elevator) => {
            myElevators.push(new StandingStill(elevator))
        })

        floors.forEach((floor) => {
            floor.on("up_button_pressed", function () { floorsWaiting.push({ floorNum: floor.floorNum(), directon: up }) })
            floor.on("down_button_pressed", function () { floorsWaiting.push({ floorNum: floor.floorNum(), directon: down }) })
        })
    },

    update: function (dt, elevators, floors) {
        myElevators.forEach((elevator, index, list) => { list[index] = elevator.update(dt, elevators, floors) });
    }
})

class Moving {
    elevator = null
    destination = null
    direction = null

    constructor(elevator, destination) {
        this.elevator = elevator
        this.destination = destination
        this.direction = getFloorDirectionFromElevator(this.elevator, destination)

        this.elevator.goToFloor(destination)
    }

    update(dt, elevators, floors) {
        console.log("Moving! ", this.direction)

        if (this.elevator.currentFloor() === this.destination) {
            console.log("\tDestination reached")
            return new StandingStill(this.elevator)
        }


        this.setIndicator()
        return this
    }

    setIndicator() {
        this.elevator.goingUpIndicator(this.direction === up)
        this.elevator.goingDownIndicator(this.direction === down)
    }
}

class StandingStill {
    elevator = null

    constructor(elevator) {
        this.elevator = elevator
    }

    update(dt, elevators, floors) {
        console.log("Standing still")
        this.elevator.goingUpIndicator(true)
        this.elevator.goingDownIndicator(true)

        const currentFloor = this.elevator.currentFloor()
        let destination = null
        if (this.elevator.getPressedFloors().length > 0) {
            console.log("\tWe got some buttons pressed")
            destination = this.getClosest(this.elevator.getPressedFloors(), currentFloor)
        } else if (floorsWaiting.length > 0) {
            console.log("\tWe got some floors calling for an elevator", floorsWaiting)
            destination = this.getClosest(floorsWaiting.map(floorWaiting => floorWaiting.floorNum), currentFloor)
            floorsWaiting.splice(floorsWaiting.indexOf(destination), 1)
        }

        if (destination === null) {
            return this
        }

        console.log("\tdestination: ", destination)
        return new Moving(this.elevator, destination)
    }

    getClosest(list, floor) {
        const closest = list.reduce((closest, evaluated) => {
            if (closest === null) {
                closest = evaluated
            }
            return getFloorsDistance(evaluated, floor) < getFloorsDistance(closest, floor) ? evaluated : closest
        })
        return closest
    }
}

function getFloorDirectionFromElevator(elevator, floorNum) {
    const position = elevator.currentFloor()

    difference = position - floorNum
    if (difference < 0) {
        return up
    } else if (difference > 0) {
        return down
    }

    return stopped
}

function getFloorsDistance(a, b) {
    return Math.abs(a - b)
}